
# test whether the tool is deterministic in learning: for each new run of
# learning the same model with same seed it should get the exact same
# reproducalbe result.


mkdir -p tmp

run() {
    n=$1
    python3 z3gi/__main__.py  -m scalable -a RegisterAutomaton -sc FIFOSet -s 1 --seed 12 &> tmp/$n.txt
}

dodiff(){
    n=$1
    command diff -U 0 -bB --minimal --strip-trailing-cr tmp/1.txt tmp/$n.txt
}    

run 1
for n in 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
do 
    echo "-------------------------------------------------------------------------"
    echo "run $n"
    echo "-------------------------------------------------------------------------"
    run $n
    dodiff $n
done    
