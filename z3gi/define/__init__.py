from abc import ABCMeta, abstractmethod

import z3

def dt_enum(name, elements):
    """define an enum_type using an algebraic datatype with given name and give elements.

       returns pair(new_enum_type, list of values in new_enum_type)
    """
    d = z3.Datatype(name)
    for element in elements:
        d.declare(element)
    d = d.create()
    return d, [d.__getattribute__(element) for element in elements]


# TODO: replacing dt_enum by declsort_enum makes it very slow!!
#  => investigate which type can best be dt_enum and which int_enum or declsort_enum

# def dt_enum(name, elements):
#     """define an enum_type using an uninterpreted sort  (see https://ericpony.github.io/z3py-tutorial/advanced-examples.htm)
#
#     """
#     # define a new uninterpreted sort (type) with given name
#     d = z3.DeclareSort(name)
#     # define constants in this new type
#     elements_of_d = [z3.Const(element, d) for element in elements]
#     return d, elements_of_d

def int_enum(name, elements):
    """ creates list of z3 integer values for given list of python integers.

        eg. a python integer 4 is used to contstruct z3.IntVal(4))
    """
    d = z3.IntSort()   # function z3.IntSort() return object representing type Int
                       # function z3.IntVal(3) creates a integer constant with value 3
                       # function z3.Int(x) creates a integer constant with name x   => value needs to be solved!
    return d, [z3.IntVal(i) for i in range(0, len(elements))]

def declsort_enum(name, elements):
    """define an enum_type using an uninterpreted sort  (see https://ericpony.github.io/z3py-tutorial/advanced-examples.htm)

    """
    # define a new uninterpreted sort (type) with given name
    d = z3.DeclareSort(name)
    # define constants in this new type
    elements_of_d = [z3.Const(element, d) for element in elements]
    return d, elements_of_d

class Z3Automaton(metaclass=ABCMeta):
    @abstractmethod
    def export(self, model):
        """Returns a z3gi.model for this automaton."""
        pass