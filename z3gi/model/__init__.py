from abc import ABCMeta, abstractmethod
from typing import List

__all__ = [
    "get_trans_acc_seq",
    "get_prefix_tree",
    "is_strongly_connected",
    "defined_formalisms",
    "Transition", "Acceptor", "NoTransitionFired", "MultipleTransitionsFired", "Transducer",
    "MutableAcceptorMixin", "MutableAutomatonMixin", "Automaton"
    ]

"""The most basic transition class available"""


class Transition():
    def __init__(self, start_state, start_label, end_state):
        # start location
        self.start_state = start_state
        # start label :  input on transition
        self.start_label = start_label
        # end location
        self.end_state = end_state

    def __str__(self, shortened=False):
        short = "{0} -> {1}".format(self.start_label, self.end_state)
        if not shortened:
            return "{0} {1}".format(self.start_state, short)
        else:
            return short


"""Exception raised when no transition can be fired"""


class NoTransitionFired(Exception):
    pass


"""Exception raised when several transitions can be fired in a deterministic machine"""


class MultipleTransitionsFired(Exception):
    pass


"""Exception raised when the automaton of the solution is invalid (can happen if non-minimal)"""


class InvalidAutomaton(Exception):
    pass


def defined_formalisms():
    """retrieves a dictionary containing the formalisms implemented and their respective classes"""
    import inspect
    sc = dict()
    crt = Automaton
    to_visit = set(crt.__subclasses__())
    while len(to_visit) > 0:
        subclass = to_visit.pop()
        if not inspect.isabstract(subclass) and not subclass.__name__.startswith("Mutable"):
            sc[subclass.__name__] = subclass
        else:
            to_visit = to_visit.union(to_visit, set(subclass.__subclasses__()))
    return sc


def sort_states(states):
    """ function to sort states

        used to get canonical order in automaton

        note: originally added to get reproducable output of this learner
              but later I found a none sorted automaton can also be used
              for reproducable output of this learner.

              The real problem was somewhere else in the learner:
                 ordering a set into a list  where
                 the ordering changed per run because the set elements
                 didn't override __eq__ and __hash__ and therefore where
                 internally ordered by the object id() which changed
                 per run because id() is internally based on the memory address allocated.

    """
    sorted_states = []
    if states and len(states) >= 1:
        sorted_states = [states[0]]
        if len(states) > 1:
            sorted_states = sorted_states + sorted(states[1:])
    return sorted_states

def sort_state_to_trans(state_to_trans,sorted_states):
    """ function to sort outgoing transitions from a state

        used to get canonical order in automaton

        note: originally added to get reproducable output of this learner
              but later I found a none sorted automaton can also be used
              for reproducable output of this learner.

              The real problem was somewhere else in the learner:
                 ordering a set into a list  where
                 the ordering changed per run because the set elements
                 didn't override __eq__ and __hash__ and therefore where
                 internally ordered by the object id() which changed
                 per run because id() is internally based on the memory address allocated.

    """
    sorted_state_to_trans = {}
    for state in sorted_states:
        if state in state_to_trans:
           sorted_state_to_trans[state] = sorted(state_to_trans[state], key=lambda t: str(t))
    return sorted_state_to_trans



"""A basic abstract automaton model"""


class Automaton(metaclass=ABCMeta):
    def __init__(self, states, state_to_trans, acc_trans_seq={}):
        super().__init__()

        # states: list of locations; normally it is a list of labels for locations
        # note: first in list is start location
        self._states = states

        # state_to_trans : dictionary where key is location and value is list of outgoing transitions
        self._state_to_trans = state_to_trans

        # keep canonical sorted representation of model
        self.keep_canonical_presentation()

        # EXTRA, because can be derivered from start location and state_to_trans,
        #        however by setting it explicitly you can set a specific access sequence for a location
        # acc_trans_seq: dictionary where key is location and value is list of transitions forming access sequence for location
        self._acc_trans_seq = acc_trans_seq

    def keep_canonical_presentation(self):
        """
          keep canonical sorted representation of model

          Important to keep a deterministic learning experience which is reproducable.
        """

        # canonical sort the model
        # * sort states
        sorted_states = sort_states(self._states)
        # * sort transitions in state2trans values
        sorted_state_to_trans= sort_state_to_trans(self._state_to_trans,sorted_states)

        # update to sorted versions
        self._states = sorted_states
        self._state_to_trans = sorted_state_to_trans



    def start_state(self):
        return self._states[0]

    def acc_trans_seq(self, state=None) -> List[Transition]:
        """returns the access sequence to a state in the form of transitions"""
        if state is not None:
            return list(self._acc_trans_seq[state])
        else:
            return dict(self._acc_trans_seq)

    def acc_seq(self, state=None):
        """returns the access sequence to a state in the form of sequences of inputs"""
        if state is not None:
            if len(self._acc_trans_seq) == 0:
                raise Exception("Access sequences haven't been defined for this machine")
            return self.trans_to_inputs(self._acc_trans_seq[state])
        else:
            return {state:self.trans_to_inputs(self._acc_trans_seq[state]) for state in self.states()}

    def states(self):
        return list(self._states)

    def transitions(self, state, label=None) -> List[Transition]:
        """ if location argument given it returns all outgoing transtions from that location,
            else it returns all transitions in the automaton
        """
        if label is None:
            return list(self._state_to_trans[state])
        else:
            return list([trans for trans in self._state_to_trans[state] if trans.start_label == label])

    def state(self, trace):
        """transition function which for given list of inputs (trace argument) returns the state reached from the start state"""
        crt_state = self.start_state()
        for symbol in trace:
            transitions = self.transitions(crt_state, symbol)
            fired_transitions = [trans for trans in transitions if trans.start_label == symbol]

            # the number of fired transitions can be more than one since we could have multiple equalities
            if len(fired_transitions) == 0:
                raise NoTransitionFired

            if len(fired_transitions) > 1:
                raise MultipleTransitionsFired

            fired_transition = fired_transitions[0]
            crt_state = fired_transition.end_state

        return crt_state

    @abstractmethod
    def trans_to_inputs(self, transitions:List[Transition])->List[object]:
        """returns a unique sequence of inputs generated by execution traversing these transitions"""
        pass

    @abstractmethod
    def inputs_to_trans(self, seq: List[object])->List[Transition]:
        """returns a unique sequence of transitions generated by execution of these inputs"""
        pass

    def input_labels(self):
        """ get set of input labels
        """
        return set([trans.start_label for trans in self.transitions(self.start_state())])

    @abstractmethod
    def output(self, trace):
        pass

    # Basic __str__ function which works for most FSMs.
    def __str__(self):
        str_rep = ""
        for (st, acc_seq) in self._acc_trans_seq.items():
            str_rep += "acc_seq("+ str(st) +") = " + " , ".join(list(map(str,acc_seq))) + "\n"
        for state in self.states():
            str_rep += str(state) + "\n"
            for tran in self.transitions(state):
                str_rep += "\t" + tran.__str__(shortened=True) + "\n"

        return str_rep


class MutableAutomatonMixin(metaclass=ABCMeta):
    """ mutable automaton mixin  which keeps model in canical form by sorting the stored states and transitions"""
    def add_state(self:Automaton, state):
        if state not in self._states:
            self._states.append(state)

        # for canonical presentation: keep states sorted
        self._states = sort_states(self._states)

    def _remove_state(self:Automaton, state):
        self._states.remove(state)
        self._state_to_trans.pop(state)

    def add_transition(self:Automaton, state, transition):
        if state not in self._state_to_trans:
            self._state_to_trans[state] = []
        self._state_to_trans[state].append(transition)

        # for canonical presentation: keep transitions sorted
        self._state_to_trans=sort_state_to_trans(self._state_to_trans, self._states)



    def generate_acc_seq(self:Automaton, remove_unreachable=True):
        """generates access sequences for an automaton. It removes states that are not reachable."""
        new_acc_seq = get_trans_acc_seq(self, from_state=self.start_state())
        for state in self.states():
            if state not in new_acc_seq and remove_unreachable:
                self._remove_state(state)

        assert(len(new_acc_seq) == len(self.states()))
        self._acc_trans_seq = new_acc_seq

    @abstractmethod
    def to_immutable(self) -> Automaton:
        pass


"""An automaton model that generates output"""


class Transducer(Automaton, metaclass=ABCMeta):
    def __init__(self, states, state_to_trans, acc_trans_seq={}):
        super().__init__(states, state_to_trans, acc_trans_seq)

    @abstractmethod
    def output(self, trace):
        """ output of last transition of trace """
        pass



"""An automaton model whose states are accepting/rejecting"""


class Acceptor(Automaton, metaclass=ABCMeta):
    def __init__(self, states, state_to_trans, state_to_acc, acc_trans_seq={}):
        super().__init__(states, state_to_trans, acc_trans_seq)

        # dictionory mapping location to boolean where True means accepting, False means rejecting
        self._state_to_acc = state_to_acc

    def is_accepting(self, state):
        """ whether state is accepting"""
        return self._state_to_acc[state]

    def accepts(self, trace):
        """ whether the trace is accepted
        """
        state = self.state(trace)
        is_acc = self.is_accepting(state)
        return is_acc

    def output(self, trace):
        """ alias for accepts(trace)
        """
        self.accepts(trace)

    def __str__(self):
        return str(self._state_to_acc) + "\n" + super().__str__()


class MutableAcceptorMixin(MutableAutomatonMixin, metaclass=ABCMeta):
    def add_state(self, state, accepts):
        super().add_state(state)
        self._state_to_acc[state] = accepts

def is_strongly_connected(aut : Automaton):
    """ strongly connected from each state each other state can be reached in the automaton"""

    # get statelist =  the list of states of the automaton
    # for a given state X
    #    generate by a depth first search through a spanning tree of states reachable from a state X
    #    then search of each state in statelist is in that spanning tree
    #    if so then each state of the automaton is reachable from state X
    #    else => BREAK because automaton is not strongly connected
    # repeat this for each state X in statelist
    # if no BREAK occured then all states are reachables from each other
    # thus the automaton is strongly connected
    states = list(aut.states())
    for state in states:
        ptree = get_prefix_tree(aut, from_state=state)
        for s in states:
            pred = lambda x: (x.state == s)
            node = ptree.find_node(pred)
            if node is None:
                return False
    return True


def get_trans_acc_seq(aut : Automaton, from_state=None):
    """Generates transition access sequences for a given automaton"""

    # first generate prefix tree (spanning tree for reaching states from start state where edges are transitions)
    ptree = get_prefix_tree(aut, from_state=from_state)
    # for each state find path through prefix tree, then the path is the access sequence for that state
    acc_seq = dict()
    states = list(aut.states())
    for state in states:
        pred = lambda x: (x.state == state)
        node = ptree.find_node(pred)
        if node is None:
            print("Could not find state {0} in tree {1} \n for model \n {2}".format(state, ptree, aut))
            continue
            # print("Could not find state {0} in tree {1} \n for model \n {2}".format(state, ptree, self))
            # raise InvalidAutomaton
        acc_seq[state] = node.path()
    return acc_seq

def get_prefix_tree(aut : Automaton, from_state=None):
    """Generates a transition prefix tree for a given automaton.
       Which is a spanning tree  of the automaton
       where
        - the nodes are the reached locations
        - the edges are transitions of the automaton

       A path from the root to some node gives the access sequence  from the start location
       to the location in that node. The access sequense is the set of traces on the path.
       """

    # does breadth first search trought automaton to visit new states
    # each  state, together with the transition which led to its visit, is added to prefix tree as a new child node
    visited  = set()
    #to_visit = set()
    to_visit = list()
    if from_state is None:
        from_state = aut.start_state()
    root = PrefixTree(from_state)
    #to_visit.add(root)
    to_visit.append(root)
    while len(to_visit) > 0:
        crt_node = to_visit.pop()
        visited.add(crt_node.state)
        transitions = aut.transitions(crt_node.state)
        for trans in transitions:
            if trans.end_state not in visited:
                child_node = PrefixTree(trans.end_state)
                crt_node.add_child(trans, child_node)
                #to_visit.add(child_node)
                to_visit.append(child_node)
    return root


class PrefixTree():
    """A transition prefix tree for an automaton.
       Which is a spanning tree  of the automaton
       where
        - the nodes are the reached locations
        - the edges are transitions of the automaton

       A path from the root to some node gives the access sequence  from the start location
       to the location in that node. The access sequense is the set of traces on the path.
    """

    def __init__(self, state):
        self.state = state
        self.tr_tree:dict = {}
        self.parent:PrefixTree = None

    def add_child(self, trans, tree):
        self.tr_tree[trans] = tree
        self.tr_tree[trans].parent = self

    def path(self) -> List[Transition]:
        if self.parent is None:
            return []
        else:
            for (tr, node) in self.parent.tr_tree.items():
                if node is self:
                    return self.parent.path()+[tr]
            raise Exception("Miscontructed tree")

    def find_node(self, predicate):
        if predicate(self):
            return self
        elif len(self.tr_tree) == 0:
            return None
        else:
            for (tran, child) in self.tr_tree.items():
                node = child.find_node(predicate)
                if node is not None:
                    return node
            return None

    def __str__(self, tabs=0):
        space = "".join(["\t" for _ in range(0, tabs)])
        tree = "(n_{0}".format(self.state)
        for (tr, node) in self.tr_tree.items():
            tree += "\n" + space + " {0} -> {1}".format(tr, node.__str__(tabs=tabs + 1))
        tree += ")"
        return tree