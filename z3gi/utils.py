import itertools
import random
from typing import List

rand = None

def set_rand_sel_seed(seed):
    global rand
    rand=random.Random(seed)

__all__ = [
    "rand_sel",
    "set_rand_sel_seed",
    "determinize"
    ]

def rand_sel(l:List):
    if not rand:
        print("ERROR: forgot to set seed!!  Use: utils.set_rand_sel_seed(42).")
        import sys
        sys.exit(1)
    return l[rand.randint(0, len(l)-1)]


class Tree(object):
    """
     Tree
     - created from traces (seq)
     - where each action within a trace gives a new edge within the tree
     - traces which the same prefix of actions follow the same path in the tree
     - where each node is uniquely index with integer increment from 0...n  where n is number of nodes in tree

    """
    def __init__(self, counter):
        self.id = next(counter)    # node id
        self.counter = counter
        self.children = {}         # edgelabel (dict key)  to child node  (dict value)   ; note: each node is a Tree
        self.parent = None

    # make tree
    def __getitem__(self, seq):
        trie = self
        for action in seq:
            if action not in trie.children:
                child = Tree(self.counter)
                child.parent = trie
                trie.children[action] = child
            trie = trie.children[action]
        return trie

    def __iter__(self):
        yield self
        for node in itertools.chain(*map(iter, self.children.values())):
            yield node

    def transitions(self):
        for node in self:
            for action in node.children:
                yield node, action, node.children[action]

    def __str__(self, indent_count=0):
        indent_space = "\t" * indent_count
        tree = "(n{0}".format(self.id)
        for action in self.children:
            tree += "\n" + indent_space + " {0} -> {1}".format(action, self.children[action].__str__(indent_count=indent_count + 1))
        tree += ")"
        return tree

    def path(self):
        seq = []
        node = self
        parent = node.parent
        while parent:
            for action in parent.children:
                if parent.children[action] == node:
                    seq.append(action)
                    break
            node = parent
            parent = node.parent
        seq.reverse()
        return seq



def determinize(seq):
    """
    Make canonical representation of a data sequence of tuples (label,value)
    so that two traces with different data values but with same path through an register automaton can
    be encoded in the same canonical form.

    Done by replacing actual values with integers starting from 0  where each value
    is uniquely mapped to an integer

      In: determinize([("a", 5), ("b", 3), ("a", 1), ("a", 3)])
      Out: [('a', 0), ('b', 1), ('a', 2), ('a', 1)]
    """
    neat = {}
    neat[None] = None
    i = 0
    for (label, value) in seq:
        if value not in neat:
            neat[value] = i
            i = i + 1
    return [(label, neat[value]) for label, value in seq]
