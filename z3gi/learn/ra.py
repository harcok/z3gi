from typing import Tuple

from learn import Learner
import z3
import model.ra
import define.ra


class RALearner(Learner):
    def __init__(self, encoder):
        super().__init__()
        if encoder is None:
            raise Exception("RAEncoder has to be supplied")

        self.encoder = encoder
        self.num_reg = None

    def newSolver(self):
        """
         create new solver

         note: we use a seed to make solver deterministic. The current commands
               to set the seed are probably not needed for our case. But set them anyway to be sure.
        """
        # make z3 verbose
        if self.z3verbose:
            z3.set_param("verbose", 10)

        # create solver and set its seed
        seed = self.seed

        # z3 -p : lists all params you can set
        #
        # default value :  get_param("smt.random_seed")  => 0  This seems to be a normal fixed seed.
        #                                                      That is: it DOESN't mean take seed from current time as is done for
        #                                                                python's random function when giving it value 0.
        #z3.set_param("smt.random_seed", seed) # HAS EFFECT for for how z3 is used by this tool!!
        #z3.set_param("sat.random_seed", seed)  # seems to have no effect for how z3 is used by this tool
        #z3.set_param("fp.spacer.random_seed", seed)  # seems to have no effect for how z3 is used by this tool
        #z3.set_param("sls.random_seed", seed)  # seems to have no effect for how z3 is used by this tool

        solver = z3.Solver()
        # solver.help() command lists all possible params you can set
        #   -> "random_seed" and "seed" seem good candidates
        #
        solver.set(random_seed=seed) # HAS EFFECT for for how z3 is used by this tool!!
        #solver.set("random_seed", seed)  # HAS EFFECT for for how z3 is used by this tool!!
        #solver.set(":random_seed", seed) # HAS EFFECT for for how z3 is used by this tool!!
        #solver.set(seed=seed)  # seems to have no effect for how z3 is used by this tool
        #solver.set(":seed", seed) # seems to have no effect for how z3 is used by this tool
        #solver.set("seed", seed) # seems to have no effect for how z3 is used by this tool

        # CONCLUSION use "random_seed" option on solver seems the most natural to use

        self.solver=solver

    def add(self, trace):
        self.encoder.add(trace)

    def set_num_reg(self, num):
        self.num_reg = num

    def model(self, min_locations=1, max_locations=15, min_registers=0, max_registers=3, ensure_min=True,
              old_definition:define.ra.Z3RegisterMachine = None, old_model:model.ra.RegisterMachine = None) -> \
            Tuple[model.ra.RegisterMachine, define.ra.Z3RegisterMachine]:

        # start with new z3 solver
        self.newSolver()

        if old_definition is not None:
            min_locations = len(old_definition.locations)
            min_registers = len(old_definition.registers) - 1

        # z3_types_for_ra_model : definition of types of register automaton in z3.
        #                         - locations,registers and actions are defined as enum types
        #                         - also the type of the output and transition function are defined
        #                           Note: the definition of these functions is going to be solved by z3.
        # z3_solution:   solution found by z3 for output and transition function
        (succ, z3_types_for_ra_model, z3_solution) = self._learn_model(min_locations=min_locations,
                                        max_locations=max_locations, min_registers=min_registers,
                                              max_registers=max_registers, ensure_min=ensure_min)

        if succ:
            # export takes
            #   - definition of types of register automaton in z3
            #   - solution of z3 as the definition for the output and transition function
            # to generate a register model (efsm) which is constructed of states with transitions with guards and update
            ra_transition_model=z3_types_for_ra_model.export(z3_solution)

            return ra_transition_model, z3_types_for_ra_model
        else:
            return None

    def _learn_model(self, min_locations, max_locations, min_registers, max_registers=3, ensure_min=True):
        """generates the definition and model for an ra whose traces include the traces added so far"""
        for num_locations in range(min_locations, max_locations + 1):
            # TODO: calculate max registers based on repeating values
            rng = range(min_registers, min(max_registers, num_locations) + 1) if self.num_reg is None else [self.num_reg]
            for num_registers in rng:
                z3_types_for_ra_model, z3_constraints = self.encoder.build(num_locations, num_registers)
                # z3_types_for_ra_model : definition of types of register automaton in z3.
                #                         - locations,registers and actions are defined as enum types
                #                         - also the type of the output and transition function are defined
                #                           Note: the definition of these functions is going to be solved by z3.
                # z3_constraints
                if self.verbose:
                   print("z3_constraints\n" + str(z3_constraints))
                self.solver.add(z3_constraints)
                if self.timeout is not None:
                    self.solver.set("timeout", self.timeout)

                result = self.solver.check()
                if self.verbose:
                    print("Learning with {0} locations and {1} registers. Result: {2}"
                      .format(num_locations, num_registers, result))
                if result == z3.sat:
                    # z3_solution:   solution found by z3 for output and transition function
                    z3_solution = self.solver.model()
                    if self.verbose:
                        print("\n\nz3 types for ra model:\n" + str(z3_types_for_ra_model))
                        print("\n\nz3 solution for transition and output function:\n" + str(z3_solution))
                    self.solver.reset()
                    return (True, z3_types_for_ra_model, z3_solution)
                else:
                    self.solver.reset()
                    if result == z3.unknown and ensure_min:
                        print("Timeout at {0} locations and {1} registers".format(num_locations, num_registers))
                        return (False, True, None)

                    # TODO: process the unsat core?
                    pass

        return  (False, False, None)